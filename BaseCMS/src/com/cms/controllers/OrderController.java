package com.cms.controllers;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cms.entity.Order;
import com.cms.service.MyBaseService;

/**
 * 
 * @author 会踢球的葡萄
 * @date 2015-12-29
 * 
 */

@Controller
@RequestMapping(value = "order")
public class OrderController extends BaseController<Order> {

	@Override
	protected String getPrefix() {
		return "order";
	}


	@Autowired
	private MyBaseService myBaseService;

	/**
	 * 功能说明：审核
	 * 
	 * @author ducc
	 * @updated
	 * @param response
	 * @param item
	 */
	@ResponseBody
	@RequestMapping(value = "audit", method = RequestMethod.POST)
	public Object add(Long id, Integer status) {
		Map<String, String> map = new HashMap<String, String>();
		try {
			myBaseService.updateByX("update Order set status=? where id=?",
					status, id);
			map.put("flag", "true");
			map.put("msg", "保存失败！");
		} catch (Exception e) {
			map.put("flag", "fasle");
			map.put("msg", "保存失败！");
			e.printStackTrace();
		}
		return map;
	}

	@Override
	protected Class<?> getClazz() {
		// TODO Auto-generated method stub
		return Order.class;
	}

}
