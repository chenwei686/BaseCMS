package com.cms.controllers;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GraphicsEnvironment;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Random;
import java.util.UUID;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.cms.service.AdminService;
import com.cms.service.DataDictService;
import com.cms.service.SmsVerifyCodeService;
import com.cms.support.QiniuFileUtil;
import com.cms.support.Result;

/**
 * @ClassName: CommonController
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangp
 * @date 2016年4月12日 上午11:04:54
 * 
 */
@Controller
public class CommonController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CommonController.class);
	@Autowired
	private DataDictService dataDictService;
	@Autowired
	private AdminService userService;
	@Autowired
	private SmsVerifyCodeService smsVerifyCodeService;

	/**
	 * 查询所有地区
	 * 
	 * @param region_code
	 * @return
	 */
	@ResponseBody
	@RequestMapping("common/listRegions")
	public Object listRegions() {
		return dataDictService.listDataByName("regionCode");
	}

	@ResponseBody
	@RequestMapping("common/listData")
	public Object listData(String name) {
		return dataDictService.listDataByName(name);
	}

	/**
	 * 文件上传 如果只是上传一个文件,则只需要MultipartFile类型接收文件即可,而且无需显式指定@RequestParam注解
	 * 如果想上传多个文件,那么这里就要用MultipartFile[]类型来接收文件,并且要指定@RequestParam注解
	 * 上传多个文件时,前台表单中的所有<input
	 * type="file"/>的name都应该是myfiles,否则参数里的myfiles无法获取到所有上传的文件
	 */
	@ResponseBody
	@RequestMapping(value = "common/fileUpload")
	public Object fileUpload(@RequestParam MultipartFile myfile,
			HttpServletRequest request) throws IOException {
		if (myfile.isEmpty()) {
			return new Result(false, "请选择文件");
		} else {
			return new Result(true, QiniuFileUtil.upload(myfile));
		}
	}

	/**
	 * 文件删除
	 */
	@ResponseBody
	@RequestMapping("common/fileDelete")
	public Object fileDelete(String path, HttpServletRequest request) {
		if (path != null) {
			QiniuFileUtil.deleteQiniuP(path);
		}
		return new Result();
	}

	@RequestMapping(value = "/common/createCode", method = RequestMethod.GET)
	public void createCode(HttpServletRequest request,
			HttpServletResponse response) {
		try {
			final int width = 120; // 定义验证码宽度
			int height = 40; // 定义验证码高度
			int codeX = 20;
			int codeY = 30;
			Float fontHeight = 30f;
			char[] codeSequence = { '0', '1', '2', '3', '4', '5', '6', '7',
					'8', '9' };
			BufferedImage bufferedImage = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_RGB);

			Graphics graphics = bufferedImage.getGraphics();
			Random random = new Random();
			graphics.setColor(new Color(250, 250, 250)); // 设置填充背景为白色
			graphics.fillRect(0, 0, width, height);
			// create the font to use. Specify the size!
			String path = CommonController.class.getClassLoader()
					.getResource("num.ttf").getPath();
			Font customFont = Font.createFont(Font.TRUETYPE_FONT,
					new File(path)).deriveFont(fontHeight);
			// register the font
			GraphicsEnvironment ge = GraphicsEnvironment
					.getLocalGraphicsEnvironment();
			ge.registerFont(Font.createFont(Font.TRUETYPE_FONT, new File(path)));
			graphics.setFont(customFont); // 设置字体样式
			graphics.setColor(Color.BLACK);
			graphics.drawRect(0, 0, width - 1, height - 1); // 描边
			// 随机产生155条干扰线，使图象中的认证码不易被其它程序探测到
			graphics.setColor(getRandColor(160, 200));
			for (int i = 0; i < 100; i++) {
				int x = random.nextInt(width);
				int y = random.nextInt(height);
				int xl = random.nextInt(12);
				int yl = random.nextInt(12);
				graphics.drawLine(x, y, x + xl, y + yl);
			}
			graphics.setColor(Color.BLACK);
			StringBuffer randomCode = new StringBuffer(); // 设置随机验证码
			int red = 0, green = 0, blue = 0;
			for (int i = 0; i < 4; i++) {
				String code = String.valueOf(codeSequence[random.nextInt(10)]);
				red = random.nextInt(255);
				green = random.nextInt(255);
				blue = random.nextInt(255);
				graphics.setColor(new Color(red, green, blue));
				graphics.drawString(code, (i + 1) * codeX, codeY); // 用随机产生的颜色将验证码绘制到图像中。
				randomCode.append(code);
			}
			HttpSession session = request.getSession(); // 将四位数字的验证码保存到Session中。
			LOGGER.info("登录验证码：" + randomCode.toString());
			session.setAttribute("loginCode", randomCode.toString());
			response.setHeader("Pragma", "no-cache");
			response.setHeader("Cache-Control", "no-cache"); // 禁止图像缓存。
			response.setDateHeader("Expires", 0);
			response.setContentType("image/jpeg");
			OutputStream sos = response.getOutputStream();
			ImageIO.write(bufferedImage, "jpeg", sos); // 将图像输出到Servlet输出流中。
			sos.close();
		} catch (Exception e) {
			e.printStackTrace();
			LOGGER.error("生成验证码异常", e.getMessage());
		}
	}

	/**
	 * 登录验证码验证
	 */
	@ResponseBody
	@RequestMapping("/common/validateCode")
	public Object validateCode(String code, HttpServletRequest request) {
		try {
			String loginCode = (String) request.getSession().getAttribute(
					"loginCode");
			if (StringUtils.isBlank(code)) {
				return new Result(false, "验证码不能为空");
			}
			code = code.trim();
			if (!code.equals(loginCode)) {
				return new Result(false, "验证码不正确");
			}
			return new Result();
		} catch (Exception e) {
			return new Result(true, "系统异常");
		}
	}

	private Color getRandColor(int fc, int bc) {// 给定范围获得随机颜色
		Random random = new Random();
		if (fc > 255)
			fc = 255;
		if (bc > 255)
			bc = 255;
		int r = fc + random.nextInt(bc - fc);
		int g = fc + random.nextInt(bc - fc);
		int b = fc + random.nextInt(bc - fc);
		return new Color(r, g, b);
	}

	@RequestMapping(value = "common/apkUpload", method = RequestMethod.GET)
	public String toApkUpload(Model model) throws IOException {
		return "apkUpload/upload";
	}

	/**
	 * 文件上传 如果只是上传一个文件,则只需要MultipartFile类型接收文件即可,而且无需显式指定@RequestParam注解
	 * 如果想上传多个文件,那么这里就要用MultipartFile[]类型来接收文件,并且要指定@RequestParam注解
	 * 上传多个文件时,前台表单中的所有<input
	 * type="file"/>的name都应该是myfiles,否则参数里的myfiles无法获取到所有上传的文件
	 */
	@ResponseBody
	@RequestMapping(value = "common/apkUpload")
	public Object apkUpload(@RequestParam MultipartFile myfile,
			HttpServletRequest request) throws IOException {
		// 上传文件的原名(即上传前的文件名字)
		if (myfile.isEmpty()) {
			return new Result(false, "请选择文件");
		} else {
			try {
				File savedDir = prepareSavedDir(request);
				myfile.transferTo(new File(savedDir, "ddpc.apk"));
				return new Result();
			} catch (Exception e) {
				e.printStackTrace();
				return new Result(false, "系统异常");
			}
		}
	}

	private File prepareSavedDir(HttpServletRequest request) throws Exception {
		File dir = new File(request.getSession().getServletContext()
				.getRealPath("/html"));
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				throw new Exception("创建保存目录失败");
			}
		}
		return dir;
	}

	/**
	 * 发送验证码
	 */
	@ResponseBody
	@RequestMapping("/api/sendCode")
	public Object sendCode(String phone) {
		try {
			if (StringUtils.isBlank(phone)) {
				return new Result(false, "号码不能为空");
			}
			String codeId = UUID.randomUUID().toString();
			smsVerifyCodeService.sendSms(phone, codeId);
			return new Result(true, codeId);
		} catch (Exception e) {
			return new Result(false, "验证码发送失败");
		}
	}

	/**
	 * 校验验证码
	 */
	@ResponseBody
	@RequestMapping("/api/verifyCode")
	public Object verifyCode(String codeId, String phone, String code) {
		try {
			return smsVerifyCodeService.verifySms(codeId, phone, code);
		} catch (Exception e) {
			return new Result(false, "系统异常,请稍后再试");
		}
	}
}
