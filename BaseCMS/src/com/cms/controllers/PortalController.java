package com.cms.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cms.entity.Article;
import com.cms.service.ArticleService;

/**
 * @ClassName: IndexController
 * @Description: TODO(这里用一句话描述这个类的作用)
 * @author zhangp
 * @date 2016年5月11日 下午5:09:49
 * 
 */
@Controller
public class PortalController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(PortalController.class);
	@Autowired
	private ArticleService articleService;

	/**
	 * 首页
	 * 
	 * @return
	 */
	@RequestMapping("/portal")
	public String index(Model model) {
		List<Article> list = articleService.list(null, null, null, null, null,
				null, null, null, 0, 10);
		model.addAttribute("list", list);
		return "portal/index";

	}

	/**
	 * 
	 * @Description: 查询文章详情
	 * @param model
	 * @param noticeId
	 * @return
	 * @throws AppException
	 * 
	 */
	@RequestMapping("/portal/article/view")
	public String info(Model model, Integer id, HttpServletRequest request) {
		model.addAttribute("article", articleService.loadById(id));
		request.getSession().setAttribute("message", "公告不存在");
		return "/portal/article/view";
	}

	/**
	 * 
	 * @Description: 查询公告详情
	 * @param model
	 * @param noticeId
	 * @return
	 * @throws AppException
	 * 
	 */
	@RequestMapping("/portal/article/more")
	public String noticeMore(Model model, HttpServletRequest request) {
		return "/portal/article/more";
	}

	@RequestMapping("/portal/article/list")
	public String list(Model model, Integer pageSize, Integer pageNo) {
		LOGGER.info("[分页查询公告初始化方法（list）][params:pageSize=" + pageSize
				+ ",pageNo=" + pageNo + "]");
		try {
			if (pageNo == null) {
				pageNo = 1;
			}
			if (pageSize == null) {
				pageSize = 15;
			}
			Long totalCount = articleService.getCount(null, null, null, null,
					null, null, null, null);
			List<Article> list = articleService.list(null, null, null, null,
					null, null, null, null, (pageNo - 1) * pageSize, pageSize);

			model.addAttribute("list", list);
			model.addAttribute("pageSize", pageSize);
			model.addAttribute("pageNo", pageNo);
			model.addAttribute("totalCount", totalCount);
		} catch (Exception e) {
			LOGGER.info("[分页查询公告初始化方法（list）][errors:" + e + "]");
		}
		return "/portal/article/list";
	}

	/**
	 * 
	 * @Description: 错误页面
	 * @param model
	 * @param noticeId
	 * @return
	 * 
	 */
	@RequestMapping("/error")
	public String error(Model model, HttpServletRequest request) {
		return "error";
	}

	/**
	 * 安全学堂
	 * 
	 * @return
	 */
	@RequestMapping("/portal/security")
	public String security(Model model) {
		return "/portal/security";
	}

	@RequestMapping("/portal/proCase")
	public String proCase(Model model) {
		return "/portal/proCase";
	}

	@RequestMapping("/portal/product")
	public String product(Model model) {
		return "/portal/product";
	}

	@RequestMapping("/portal/success/CUP")
	public String CUP(Model model) {
		return "/portal/success/CUP";
	}

	@RequestMapping("/portal/success/Financial")
	public String Financial(Model model) {
		return "/portal/success/Financial";
	}

	@RequestMapping("/portal/success/phoneQQ")
	public String phoneQQ(Model model) {
		return "/portal/success/phoneQQ";
	}

	@RequestMapping("/portal/success/yaan")
	public String yaan(Model model) {
		return "/portal/success/yaan";
	}

	@RequestMapping("/portal/success/lianhui")
	public String lianhui(Model model) {
		return "/portal/success/lianhui";
	}

	@RequestMapping("/portal/about")
	public String service(Model model) {
		return "/portal/about";
	}

	@RequestMapping("/portal/zpos")
	public String zpos(Model model) {
		return "/portal/zpos";
	}

	@RequestMapping("/portal/support")
	public String support(Model model) {
		return "/portal/support";
	}
}
