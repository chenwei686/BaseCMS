package com.cms.controllers;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.cms.entity.Bus;
import com.cms.service.MyBaseService;
import com.cms.support.Result;

@Controller
@RequestMapping(value = "api")
public class BusApiController {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BusApiController.class);
	@Autowired
	private MyBaseService myBaseService;

	// 订单列表
	@ResponseBody
	@RequestMapping(value = "/bus/list")
	public Object list(Bus bus, Integer pageSize, Integer pageNum) {
		LOGGER.info(bus.toString());
		try {
			List<Bus> list = myBaseService.listByExample(bus,pageNum , pageSize);
			return new Result(true, list);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "系统异常,请稍后再试");
		}
	}

	// 订单详情
	@ResponseBody
	@RequestMapping(value = "/bus/detail")
	public Object detail(Long id) {
		try {
			Bus bus = (Bus) myBaseService.get(Bus.class, id);
			return new Result(true, bus);
		} catch (Exception e) {
			e.printStackTrace();
			return new Result(false, "系统异常,请稍后再试");
		}
	}

}
