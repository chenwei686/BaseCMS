package com.cms.service;

import java.util.List;

import com.cms.entity.Order;
import com.cms.support.Result;

public interface OrderService extends BaseService {
	public Result save(Order entity);

	public Result updateStatus(Integer orderId, Integer status);

	public List<Order> list(String userName, Integer pageSize, Integer pageNum);
	
	public Boolean hasNoDoneOrder(String userName) ;
}
