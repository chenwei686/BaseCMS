package com.cms.service;

import java.util.List;

import com.cms.dto.UserReqDto;
import com.cms.dto.UserRespDto;
import com.cms.entity.User;
import com.cms.support.Result;

public interface UserService {
	public Result add(User user);

	public Result update(User user);

	public Result delete(String id);

	public User get(String id);

	public UserRespDto list(UserReqDto user, Integer from, Integer length);

	public List<User> getUsers(String userStr);
}
