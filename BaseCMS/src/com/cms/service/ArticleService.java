package com.cms.service;

import java.util.Date;
import java.util.List;

import com.cms.entity.Article;

public interface ArticleService {

	public Article loadById(Integer id);

	public List<Article> list(String wxAccount,String title, String status,String remark, String content,Integer catgoryId, Date startDate,Date endDate,Integer from, Integer to);

	public Long getCount(String wxAccount,String title, String status,String remark,String content,Integer catgoryId, Date startDate,Date endDate);

	public Article add(Article entity);

	public boolean modify(Article entity);

	public boolean delete(Integer id);
}