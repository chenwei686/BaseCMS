package com.cms.support;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cms.entity.DataDict;

/**
 * @author: zhangp Date: 14-7-16 Time: 上午9:55.
 */
public class BaseRadioTag extends BaseDiyTag {
	private static final long serialVersionUID = 8248756476934340948L;
	private String checkedValue;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseRadioTag.class);
	public int doStartTag() throws JspException {
		StringBuilder sb = new StringBuilder();
		List<DataDict> dataDicts = dataDictService.listDataByName(collection);

		for (DataDict dataDict : dataDicts) {
			String id = name + "_" + dataDict.getValue();
			sb.append(" <input type='radio' name='").append(name)
					.append("' id='").append(id).append("'").append(" value='")
					.append(dataDict.getValue()).append("'");
			if (checkedValue != null
					&& checkedValue.equals(dataDict.getValue())) {
				sb.append(" checked='checked' ");
			}

			generateAttribute(sb);
			sb.append(" /> ").append("<label for='").append(id).append("'>")
					.append(dataDict.getDescription())
					.append("</label> &nbsp;");
		}
		try {
			pageContext.getOut().print(sb.toString());
		} catch (IOException e) {
			LOGGER.error("类（BaseRadioTag）的 方法（doStartTag）异常" + e);
		}
		return EVAL_BODY_INCLUDE;
	}

	public String getCheckedValue() {
		return checkedValue;
	}

	public void setCheckedValue(String checkedValue) {
		this.checkedValue = checkedValue;
	}

}
