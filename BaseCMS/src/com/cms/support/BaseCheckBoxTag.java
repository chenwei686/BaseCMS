package com.cms.support;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.cms.entity.DataDict;

/**
 * @author: zhangp Date: 14-7-16 Time: 上午9:55.
 */
public class BaseCheckBoxTag extends BaseDiyTag {

	private static final long serialVersionUID = 1L;
	private String checkedValues;
	private static final Logger LOGGER = LoggerFactory
			.getLogger(BaseCheckBoxTag.class);
	public int doStartTag() {
		StringBuilder sb = new StringBuilder();

		List<DataDict> dataDicts = dataDictService.listDataByName(collection);

		for (DataDict dataDict : dataDicts) {
			String id = name + "_" + dataDict.getValue();
			sb.append("<input type='checkbox' name='").append(name)
					.append("' id='").append(id).append("'").append(" value='")
					.append(dataDict.getValue()).append("'");
			if (checkedValues != null
					&& checkedValues.contains(dataDict.getValue())) {
				sb.append(" checked='checked' ");
			}
			generateAttribute(sb);
			sb.append(" /> ").append("<label for='").append(id).append("'>")
					.append(dataDict.getDescription())
					.append("</label> &nbsp; ");
		}
		try {
			pageContext.getOut().print(sb.toString());
		} catch (IOException e) {
			LOGGER.error("类（BaseCheckBoxTag）的 方法（doStartTag）异常" + e);
		}
		return EVAL_BODY_INCLUDE;
	}

	public String getCheckedValues() {
		return checkedValues;
	}

	public void setCheckedValues(String checkedValues) {
		this.checkedValues = checkedValues;
	}

}
