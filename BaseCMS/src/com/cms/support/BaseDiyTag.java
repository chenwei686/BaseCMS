package com.cms.support;


import javax.servlet.jsp.tagext.TagSupport;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.cms.service.DataDictService;

/**
 * @author: zhangp
 * Date: 14-7-16
 * Time: 上午9:55.
 */
public class BaseDiyTag extends TagSupport {
    private static final long serialVersionUID = -1401431096999186953L;
    private static ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(
			"applicationContext-hibernate.xml");
	protected DataDictService dataDictService = (DataDictService) applicationContext
			.getBean("dataDictServiceImpl");
	
    protected String name;
    protected String collection;
    protected String type;
    protected String id;
    protected String onclick;
    protected String onfocus;
    protected String onblur;
    protected String onchange;
    protected String onmouseover;
    protected String onmouseout;
    protected String disabled;
    protected String cssStyle;
    protected String cssClass;
    protected String size;

    public void generateAttribute(StringBuilder sb) {
        if (id != null) {
            sb.append(" id='").append(id).append("'");
        }
        if (onclick != null) {
            sb.append(" onclick='").append(onclick).append("'");
        }
        if (onfocus != null) {
            sb.append(" onfocus='").append(onfocus.replace("'", "\"")).append("'");
        }
        if (onblur != null) {
            sb.append(" onblur='").append(onblur.replace("'", "\"")).append("'");
        }
        if (onchange != null) {
            sb.append(" onchange='").append(onchange.replace("'", "\"")).append("'");
        }
        if (onmouseover != null) {
        	sb.append(" onmouseover='").append(onmouseover.replace("'", "\"")).append("'");
        }
        if (onmouseout != null) {
        	sb.append(" onmouseout='").append(onmouseout.replace("'", "\"")).append("'");
        }
        if (disabled != null) {
        	sb.append(" disabled='").append(disabled.replace("'", "\"")).append("'");
        }
        if (cssStyle != null) {
            sb.append(" style='").append(cssStyle).append("'");
        }
        if (cssClass != null) {
            sb.append(" class='").append(cssClass).append("'");
        }
        if (size != null) {
            sb.append(" size='").append(size).append("'");
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    
    
    public String getCollection() {
		return collection;
	}

	public void setCollection(String collection) {
		this.collection = collection;
	}

	public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }

    public String getOnclick() {
        return onclick;
    }

    public void setOnclick(String onclick) {
        this.onclick = onclick;
    }

    public String getOnfocus() {
        return onfocus;
    }

    public void setOnfocus(String onfocus) {
        this.onfocus = onfocus;
    }

    public String getOnblur() {
        return onblur;
    }

    public void setOnblur(String onblur) {
        this.onblur = onblur;
    }

    public String getOnchange() {
        return onchange;
    }

    public void setOnchange(String onchange) {
        this.onchange = onchange;
    }

    public String getCssStyle() {
        return cssStyle;
    }

    public void setCssStyle(String cssStyle) {
        this.cssStyle = cssStyle;
    }

    public String getCssClass() {
        return cssClass;
    }

    public void setCssClass(String cssClass) {
        this.cssClass = cssClass;
    }

	public String getOnmouseover() {
		return onmouseover;
	}

	public void setOnmouseover(String onmouseover) {
		this.onmouseover = onmouseover;
	}

	public String getOnmouseout() {
		return onmouseout;
	}

	public void setOnmouseout(String onmouseout) {
		this.onmouseout = onmouseout;
	}

	public String getDisabled() {
		return disabled;
	}

	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}
    
}
