package com.cms.common.pojo;

/** 响应信息. */
public class Result {

	/** 状态（正数成功，负数失败，1-普通成功、-1-普通失败） */
	private int state;
	/** 结果数据，如果state为失败，则此值失败提示（字符串） */
	private Object datas;
	
	public Result() {}
	
	public Result(int state, Object datas) {
		this.state = state;
		this.datas = datas;
	}
	
	/** 创建一个失败结果对象. */
	public static Result error(String errorMsg) {
		Result result = new Result();
		result.setState(-1);
		result.setDatas(errorMsg);
		return result;
	}
	
	/** 创建一个成功结果对象. */
	public static Result success(Object datas) {
		Result result = new Result();
		result.setState(1);
		result.setDatas(datas);
		return result;
	}

	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public Object getDatas() {
		return datas;
	}
	public void setDatas(Object datas) {
		this.datas = datas;
	}
}