package com.cms.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "t_cms_article")
public class Article implements Serializable {
	private static final long serialVersionUID = 1L;

	private Integer id;
	private String title;
	private String remark;
	private String content;
	private Integer showOrder;
	private Integer catgoryId;
	private String status;
	private Date modifyDate;

	private String wxAccount;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@Column
	public Integer getShowOrder() {
		return showOrder;
	}

	public void setShowOrder(Integer showOrder) {
		this.showOrder = showOrder;
	}

	@Column
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "content", length = 5000, nullable = true)
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Column
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	@Column
	public Integer getCatgoryId() {
		return catgoryId;
	}

	public void setCatgoryId(Integer catgoryId) {
		this.catgoryId = catgoryId;
	}

	@Column
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Column
	public Date getModifyDate() {
		return modifyDate;
	}

	public void setModifyDate(Date modifyDate) {
		this.modifyDate = modifyDate;
	}

	public String getWxAccount() {
		return wxAccount;
	}

	public void setWxAccount(String wxAccount) {
		this.wxAccount = wxAccount;
	}

	@Override
	public String toString() {
		return "Article [id=" + id + ", title=" + title + ", remark=" + remark
				+ ", content=" + content + ", showOrder=" + showOrder
				+ ", catgoryId=" + catgoryId + ", status=" + status
				+ ", modifyDate=" + modifyDate + ", wxAccount=" + wxAccount
				+ "]";
	}

}
