package com.cms.entity;

import java.text.ParseException;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.cms.common.hibernate.BaseEntity;

@Entity
@Table(name = "t_cms_order")
public class Order extends BaseEntity {
	private static final long serialVersionUID = 1L;
	@Column
	private Long busId;
	@Column
	private String userId;
	@Column
	private Integer status;// 0待审核，1已取消，2审核通过,3审核拒绝
	@Column
	private String fromAddr;
	@Column
	private String toAddr;
	@Column
	private String goDate;
	@Column
	private Double price;
	@Column
	private Integer seats;

	@Transient
	public String getStatusName() throws ParseException {
		String statusName = "待审核";
		if (status != null) {
			if (status == 1) {
				statusName = "已取消";
			}
			if (status == 2) {
				statusName = "审核通过";
			}
			if (status == 3) {
				statusName = "审核拒绝";
			}
		}

		return statusName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getFromAddr() {
		return fromAddr;
	}

	public void setFromAddr(String fromAddr) {
		this.fromAddr = fromAddr;
	}

	public String getToAddr() {
		return toAddr;
	}

	public void setToAddr(String toAddr) {
		this.toAddr = toAddr;
	}

	public String getGoDate() {
		return goDate;
	}

	public void setGoDate(String goDate) {
		this.goDate = goDate;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Long getBusId() {
		return busId;
	}

	public void setBusId(Long busId) {
		this.busId = busId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public Integer getSeats() {
		return seats;
	}

	public void setSeats(Integer seats) {
		this.seats = seats;
	}

	@Override
	public String toString() {
		return "Order [busId=" + busId + ", userId=" + userId + ", status="
				+ status + ", fromAddr=" + fromAddr + ", toAddr=" + toAddr
				+ ", goDate=" + goDate + ", price=" + price + ", seats="
				+ seats + "]";
	}
	
}
