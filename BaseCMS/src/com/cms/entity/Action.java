package com.cms.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.alibaba.fastjson.annotation.JSONField;

@Entity
@Table(name = "t_cms_action")
public class Action implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String title;
	private String url;
	private Action parent;
	private Set<Action> children;
	private Set<Role> roles;
	private boolean valid;
	private String icon;// 图标
	@SuppressWarnings("unused")
	private String parentName;
	@SuppressWarnings("unused")
	private Integer parentId;

	@Id
	@Column
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Column
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column
	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	@JSONField(serialize = false)
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "parent_id")
	public Action getParent() {
		return parent;
	}

	public void setParent(Action parent) {
		this.parent = parent;
	}

	@OneToMany(mappedBy = "parent", fetch = FetchType.EAGER)
	@OrderBy("id")
	public Set<Action> getChildren() {
		return children;
	}

	public void setChildren(Set<Action> children) {
		this.children = children;
	}

	@JSONField(serialize = false)
	@ManyToMany(mappedBy = "actions", fetch = FetchType.EAGER)
	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@JSONField(name = "checked")
	@Transient
	public boolean isValid() {
		return valid;
	}

	public void setValid(boolean valid) {
		this.valid = valid;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Action other = (Action) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Column
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	@Transient
	public String getParentName() {
		if (parent != null) {
			return parent.getTitle();
		} else {
			return "";
		}
	}
    
	public void setParentName(String parentName) {
		this.parentName = parentName;
	}
	@Transient
	public Integer getParentId() {
		if (parent != null) {
			return parent.getId();
		} else {
			return null;
		}
	}

	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

}
