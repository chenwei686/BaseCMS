package com.cms.entity.weixin;

import java.util.ArrayList;
import java.util.List;


/** 
 * @ClassName: MsgNewsVO 
 * @Description: TODO(图文消息) 
 * @author zhangp 
 * @date 2016年5月9日 下午1:58:40 
 *  
 */
public class MsgNewsVO {

	private String createTimeStr;
	private List<MsgNews> msgNewsList = new ArrayList<MsgNews>();
	
	
	public String getCreateTimeStr() {
		return createTimeStr;
	}
	public void setCreateTimeStr(String createTimeStr) {
		this.createTimeStr = createTimeStr;
	}
	public List<MsgNews> getMsgNewsList() {
		return msgNewsList;
	}
	public void setMsgNewsList(List<MsgNews> msgNewsList) {
		this.msgNewsList = msgNewsList;
	}
	
	
	
}