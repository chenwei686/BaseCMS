<%@ taglib prefix="myfn"
	uri="http://www.cms.com/CMS/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	padding-left: 40px;
	line-height: 30px;
	text-align: left;
}
</style>
</head>
<body>
	<div class="table-container" id="content">
		<form   id="form">
			<table class="table table-bordered">
				<tr>
					<td>角色标识:</td>
					<td><input name="authority" value="ROLE_" id="authority" class="form-control" /></td>
					<td></td>
				</tr>
				<tr>
					<td>角色说明:</td>
					<td><input name="description" id="description" class="form-control" /></td>
					<td></td>
				</tr>
			</table>
			<p>权限:</p>
			<table class="table table-bordered">
				<c:forEach items="${action_root.children}" var="action">
					<tr>
						<td><h2>${action.title}</h2></td>
					</tr>
					<c:forEach items="${action.children}" var="child">
						<tr>
							<td><b>${child.title}</b> <c:forEach
									items="${child.children}" var="pchild">
									<input name="actionIds" type="checkbox" value="${pchild.id}"
										<c:if test="${myfn:contains(role.actions, pchild)}">checked='checked'</c:if>>${pchild.title}
			                 </c:forEach></td>
						</tr>
					</c:forEach>
				</c:forEach>
			</table>
			<input type="button" value="保存" class="btn btn-default" onclick="doSubmit();"> 
		</form>
	</div>
	<script type="text/javascript">
		function doSubmit() {
			if($("#authority").val()==""){
				layer.alert("角色标识不能为空");
				return;
			}
			if($("#description").val()==""){
				layer.alert("角色说明不能为空");
				return;
			}
			if($(':checked').length == 0){
				layer.alert("角色权限不能为空");
				return;
			}
			var data = $("#form").serialize();
			$.ajax({
				url : "add",
				data : data,
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						layer.alert('操作成功', {
							icon : 6
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); //获取窗口索引
									parent.layer.close(index);
								});
						parent.reload();
					} else {
						layer.alert('操作失败:' + result.obj);
					}
				},
				error : function() {
					layer.alert('系统异常');
				}
			});
		}
	</script>
</body>
</html>