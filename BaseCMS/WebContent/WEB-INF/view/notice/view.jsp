<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="myfn"
	uri="http://www.cms.com/CMS/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/header.jsp" %>
<style type="text/css">
	.left_div {
		width:100px;
		padding: 10px;
		float: left;
	}
	.right_div {
		padding: 10px;
		float: left;
		width:400px;
	}
</style>
</head>
<body>
<div class="form-container">
		<div>
			<span class="title">公共服务</span>
			<img src="<c:url value="/images/jiantou.png" />" />
			<span class="sec-title">公告管理</span>
			<img src="<c:url value="/images/jiantou.png" />" />
			<span class="sec-title">公告详情</span>
		</div>
		<hr />
	</div>
	<div id="main" style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form action="notice/update" method="post" id="updateNoticeForm"
				enctype="multipart/form-data" class="form-inline form-index">
				<div class="form-group">
					<label class="control-label">标题:</label>
					<input class="form-control" name="title" id="title" style="width: 300px" value="${notice.title }" readonly="readonly"/>
					<input name="id" id="id" value="${notice.id }" type="hidden">
				</div>
				<br />
				<div class="form-group">
					<label class="control-label">状态:</label>
					<select class="form-control" name=status id="status" disabled="disabled">
							<c:forEach items="${STATEMAP }" var="map">
								<option value="${map.key}">${map.value}</option>
							</c:forEach>
					</select>
				</div>
				<br />
				<div class="form-group">
					<label class="control-label">公告类型:</label>
					<html:select cssClass="form-control" id="type" name="type" disabled = "disabled" collection="noticeType" selectValue="${notice.type }">
					</html:select>
				</div>
				<br />
				<div class="form-group">
					<label class="control-label">公告内容:</label>
				</div>
				<div class="form-group">
					${notice.contents }
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript">
		$(function() {
			$("#type option[value=${notice.type}]").attr(
				"selected", "selected");
			$("#status option[value=${notice.status}]").attr(
					"selected", "selected");
		});
		function goBack(url) {
			window.parent.document.getElementById('leftFrame').contentWindow.document
					.getElementById(url).click();
		}
		function modifyNotice(){
			if ($('#title').val() == "") {
				$.messager.alert("操作提示", "请输入标题！", "error");
				$('#title').focus();
				return false;
			}
			if ($('#type').val() == "") {
				$.messager.alert("操作提示", "请选择类型！", "error");
				$('#type').focus();
				return false;
			}
			if ($('#contents').val() == "") {
				$.messager.alert("操作提示", "请输入内容！", "error");
				$('#contents').focus();
				return false;
			}
			var params = $("#updateNoticeForm").serialize();
			params = decodeURIComponent(params, true);
			$.ajax({
				url : "modify",
				data : params,
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						$.messager.alert("提示","操作成功","info",function(){
							window.location.href = "notice";
						});
					} else {
						$.messager.alert("提示","操作失败：" + result.message);
					}
				},
				/* error : function(result) {
					$.messager.alert("提示","操作失败：" + result.message);
				} */
				error:function(){
					$.messager.alert("提示","您没有该权限,请联系管理员","error");
				}
			});
		}
	</script>
</body>
</html>