<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>卡卡付-关于我们</title>
<%@ include file="/WEB-INF/view/portal/head.jsp"%>
<link rel="stylesheet" href="<c:url value="/css/portal/about.css"/>"/>
</head>
<body>
	<div class="navbar-pay">
		<div class="nav_width">
			<div class="container">
				<a class="navbar-brand" href="<c:url value="/"/>"><img
					src="<c:url value="/images/portal/one/ikkpay_1.png"/>" class="logo" /></a>
				<ul>
					<li><a href="<c:url value="/portal"/>" >主页</a></li>
					<li><a href="<c:url value="/portal/product"/>">产品展示</a></li>
					<li><a href="<c:url value="/portal/proCase"/>">成功案例</a></li>
					<li><a href="<c:url value="/portal/security"/>">安全保障</a></li>
					<li><a href="<c:url value="/portal/support"/>">技术支持</a></li>
					<li><a href="<c:url value="/portal/about"/>" class="active">关于我们</a></li>
				</ul>
			</div>
		</div>
	</div>
	<div class="topImg">
		<img src="<c:url value="/images/portal/about/about.png"/>" />
	</div>
	<div class="serDiv">
		<div class="container ser-container">
			<div class="title">成都卡卡付科技有限公司</div>
			<div class="content">成都卡卡付科技有限公司成立于2015年，是一家致力于支付创新的科技公司，系成都中联信通科技股份有限公司（简称“中联信通”，股票代码“831495”）控股子公司。</div>
			<div class="content">中联信通是国家移动支付标准制定的参与者，拥有10年的嵌入式软件及移动支付研究经验，自主研发出国内领先的安全支付中间件及WEB应用中间件，首创了NFC手机与金融IC卡移动金融服务
				框架并成功应用于“银联手机支付”平台、“卡卡联”产品及金融IC卡公共服务平台等系统。中联信通是中国银联手机支付业务全国平台运营合作伙伴，
				是经国家相关部门认证的“高新技术企业“、“软件企业”，先后通过ISO9001质量管理体系及国际标准组织SEI的CMMI3级SCAMPI
				Class A 正式评估， 拥有31项软件著作权，先后获得国家及省市多项专线及政策支持，已有多项国家级科研项目通过验收。</div>
			<div class="content">卡卡付秉承中联信通优秀的技术基因和银联合作背景，专注于支付创新，自主搭建了支付平台并推出了一系列支付产品，包括NFC支付、认证支付、网银支付、协议支付、委托付款等。并提供线上、线下聚合支付服务以及整体的行业支付解决方案。</div>
			<div class="content">公司聚焦保险、互联网金融、电力、彩票、游戏等行业，为客户提供有特色、定制化的移动支付与互联网支付产品和解决方案。公司已在移动社交平台、游戏集成平台、互联网金融理财、智慧农村、跨银行资金归集、互联网购彩、保险、地方税收等领域提供的支付服务。</div>
			<div class="content">目前公司已与腾讯、当乐网、联汇通宝、四川福彩、航天金穗、等知名企业展开了合作。</div>
			<div class="content">公司拥有一支精干、高效、专业扎实、朝气蓬勃的管理、技术、风控、市场拓展队伍，招揽了来自知名互联网企业、资深支付公司人才的加盟。公司以“立足西部、辐射全国”为战略部署，力争三年内成为一家规模适度、机制灵活、具有核心竞争力的西部最大的第三方移动支付公司。</div>

			<div class="title" style="margin-top: 40px;">联系我们</div>
			<div class="content">
				7×24小时全天候服务热线：<span style="color: #14B659; font-size: 20px;">4000-365-724</span>
			</div>
			<div class="content">
				公司地址：<b>四川省成都市高新区天府大道中段1268号1号楼E3栋6层1-7号</b>
			</div>
		</div>
	</div>

	<div class="footer">
		<div class="Copyright">Copyright 版权所有：成都卡卡付科技有限公司 All Right
			Reserve 蜀ICP备16003612号</div>
	</div>
</body>
</html>
