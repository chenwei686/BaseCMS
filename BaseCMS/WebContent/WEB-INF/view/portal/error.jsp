<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page pageEncoding="UTF-8"%>
<%
	String path = request.getContextPath();
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge,Chrome=1" />
<meta http-equiv="X-UA-Compatible" content="IE=9" />
<meta name="renderer" content="webkit" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>卡卡付-页面找不到了</title>
<link rel="stylesheet"
	href="<c:url value="/js/bootstrap/css/bootstrap.min.css"/>" />
<!--[if lt IE 9]>
  <script src="http://cdn.bootcss.com/html5shiv/3.7.2/html5shiv.min.js"></script>
  <script src="http://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" href="<c:url value="/css/common.css"/>" />
<link rel="stylesheet" href="<c:url value="/css/announce.css"/>" />
<script src="<c:url value="/js/JQuery/jquery-1.10.0.min.js"/>"></script>
<script src="<c:url value="/js/bootstrap/js/bootstrap.min.js"/>"></script>

<style>
.img {
	margin-top: 180px;
	text-align: center;
}

.img .reason {
	color: #8B9596;
	font-size: 22px;
	margin-top: 30px;
	margin-bottom: 20px;
}

.img #jump_time {
	color: red;
}

.img .index a,.img .index a:hover {
	color: #FEAA27;
}
</style>
</head>
<body>
	<div class="navbar-pay">
		<div class="top">
			<div class="container">
				<div>
					<a href="https://b.kklpay.com/" target="_blank">商户登录</a><span
						style="color: #9E9D98;"> | </span> <a href="https://s.kklpay.com/"
						target="_blank">分销商登录</a> <span class="phone">4000-365-724</span>
				</div>
			</div>
		</div>
		<div class="nav_width" style="border-bottom: 1px solid #EFECEC;">
			<div class="container">
				<a class="navbar-brand" href="<c:url value="/"/>"><img
					src="<c:url value="/images/one/ikkpay_1.png"/>" class="logo" /></a>
				<ul>
					<li><a href="<c:url value="/"/>" class="active">主页</a></li>
					<li><a href="<c:url value="/product"/>">产品展示</a></li>
					<li><a href="<c:url value="/proCase"/>">成功案例</a></li>
					<li><a href="<c:url value="/security"/>">安全保障</a></li>
					<!-- <li><a href="<c:url value="/service"/>" >技术支持</a></li> -->
					<li><a href="<c:url value="/service"/>">关于我们</a></li>
				</ul>
			</div>
		</div>
	</div>
	<!-- 
	<nav class="navbar navbar-default navbar-sec">
		<div class="container">
			<ul class="nav navbar-nav nav-sec">
				<li><a href="<c:url value="/"/>"></a></li>
			</ul>
		</div>
	</nav> -->

	<div class="img">
		<img src="<c:url value="/images/404-13.png"/>" />
		<div class="reason">您访问的页面找不到了</div>
		<div class="index">
			<span id="jump_time">10</span>秒后返回 <a href="<%=path%>/">主页</a>
		</div>
	</div>

	<div class="footer">
		<div class="Copyright">Copyright 版权所有：成都卡卡付科技有限公司 All Right
			Reserve 蜀ICP备16003612号</div>
	</div>

	<script>
    	var conHeight = $('.sys-container').height();
    	var nowHeight = $(window).height() - 263;
    	if(conHeight<nowHeight){
    		$('.footer').addClass("navbar-fixed-bottom");
    	}
    	
    if (top.location != self.location){
		top.location = self.location;
    }
	$(function(){
		countDown(10);
	});
	
	function countDown(secs){        
		 var jumpTo = document.getElementById("jump_time");
		 jumpTo.innerHTML=secs;  
		 if(--secs>0){     
		     setTimeout("countDown("+secs+")",1000);     
		    }     
		 else{ 
			 window.open('<%=path%>
		/', '_self');
			}
		}
	</script>
</body>
</html>
