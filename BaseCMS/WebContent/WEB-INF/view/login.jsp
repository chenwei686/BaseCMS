<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page  import="java.util.Date" %>  
<%@ page pageEncoding="UTF-8"%>
<head>
<title>登录</title>
<%@ include file="/WEB-INF/view/header.jsp" %>
<style>
body {
	background:url("<c:url value="/images/bg1.png"/>");
}
.img-out {
	text-align: center;
	margin-top: 6%;
	margin-bottom: 30px;
}
.btn-primary:hover,
.btn-primary:focus,
.btn-primary {
	margin-top:20px;
	background-color:#67BA2F;
	border-color:#67BA2F;
	height:48px;
	text-align:right;
	width: 338px;
}
.login-container form .input-out {
	position: relative;
	margin-bottom: 10px;
}
.login-container .form-class {
	margin:0 auto;
	width:356px;
}
.login-container form span {
	position: absolute; 
	font-size: 14px;
	left: 13px;
	top: 13px;
	color:#BAC7D7;
}
.login-container form .form-control {
	width:338px;
	padding-left: 70px;
	height:45px;
}
form button img {
	margin-right: 18px;
	width: 32px;
}
.footer p {
	color:#CBCBCB;
}
.login-container form .input-out .error {
	color:red;
}
</style>
<script type="text/javascript">
$(function() {
	document.title='CMS后台管理系统';
});
</script>
</head>

<body>
	<div class="container login-container">
		<div class="row">
			<div class="img-out">
				<img src="<c:url value="/images/1.png"/>" />
			</div>
			<div class="form-class">
				<form class="form-signin" role="form" method="POST" action="j_spring_security_check" name="f" id="loginForm">
					<div class="input-out">
						<span>用户名</span>
						<input type="email" class="form-control" name="j_username" id="j_username" autofocus>
					</div>
					<div class="input-out">
						<span>密码</span>
						<input type="password" class="form-control" name="j_password" id="j_password">
					</div>
					<div class="input-out">
						<span>验证码</span>
						<input type="text" class="form-control" style="width:200px; display:inline;margin-right: 10px;" autofocus id="code" name="code" maxlength="4" onkeyup="this.value=this.value.replace(/\D/g,'')" onafterpaste="this.value=this.value.replace(/\D/g,'')">
						<img height="40px" width="120px"  id="flushCode" src="" onclick="flush();" alt="验证码" />
					</div>
					<button class="btn btn-primary btn-block" onclick="submitDo();return false;"><img src="<c:url value="/images/arrow2.png"/>" /></button>
				<div class="input-out">
					<c:if test="${param['error']=='01' }">
						<span class="error">用户名或密码错误</span>
					</c:if>
					<c:if test="${param['error']=='02' }">
						<span class="error">证书无效</span>
					</c:if>
					<c:if test="${param['error']=='03' }">
						<span class="error">用户已被锁定或注销</span>
					</c:if>
					<c:if test="${param['error']=='04' }">
						<span class="error">链接超时或账号在其他主机登录</span>
					</c:if>
					<div style="color: red; margin: 15px;" id="messageError"></div>
					</div>
				</form> 
			</div>
		</div>
	</div>
<!-- 	<div class="footer">
        <p>版本信息 CMS后台管理系统1.0</p>
    </div> -->
	<script  type="text/javascript">
	if (top.location != self.location){
		top.location = self.location;
	}
	var dateTime =null;
	function flush(){
		var path = "<%=request.getContextPath()%>";
		if(dateTime==null){
			dateTime = "<%=new Date().getTime()%>";
			dateTime = dateTime-1;
		}else{
			dateTime = dateTime-1;
		}
		$("#flushCode").attr("src",path+"/common/createCode?t="+dateTime);
	}
	$(function() {
		flush();
	});
	function submitDo(){
		if($("#j_username").val()==''){
			$("#alert-content").html("用户名不能为空");
			$("#alert-modal").modal("show");
			return false;
		}
		if($("#j_password").val()==''){
			$("#alert-content").html("密码不能为空");
			$("#alert-modal").modal("show");
			return false;
		}
		var code = $("#code").val();
		if(code==''){
			$("#alert-content").html("验证不能为空");
			$("#alert-modal").modal("show");
			return false;
		}
		if(code.length!=4){
			$("#alert-content").html("验证输入不正确");
			$("#alert-modal").modal("show");
			return false;
		}
		$.ajax({
			url:'common/validateCode',
	   		type:'post',
			data : {
				code : code
			},
			cache : false,
			success : function(html) {
				if(html.success){
					$("#messageError").text("");
					$('#loginForm').submit();
				}else{
					$("#messageError").text("验证码错误");
					return false;
				}
			},
			error:function(){
				$.messager.alert("提示","您没有该权限,请联系管理员","error");
			}
		});
	}
</script>
</body>
</html>



