<%@ taglib prefix="myfn" uri="http://www.cms.com/CMS/functions"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="/tld/html-tags" prefix="html"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<%@ include file="/WEB-INF/view/head.jsp"%>
<style>
.table-bordered {
	text-align: left;
}

.table-bordered>tbody tr:nth-child(odd) {
	background: #fff;
}

.table-bordered>tbody tr td {
	line-height: 30px;
}

.dd {
	width: 200px;
}
</style>
</head>
<body>
	<div id="main"
		style="margin-left: auto; margin-right: auto; width: 980">
		<div class="table-container" id="content">
			<form id="form">
				<table class="table table-bordered">
					<tr>
						<td>中文名称:</td>
						<td><input name="nameCN" id="nameCN" class="form-control" /></td>
					</tr>
					<tr>
						<td>英文名称:</td>
						<td><input name="name" id="name" class="form-control" /></td>
					</tr>
					<tr>
						<td>字段值:</td>
						<td><input name="value" id="value" class="form-control" /></td>
					</tr>
					<tr>
						<td>字段描述:</td>
						<td><input name="description" id="description" class="form-control" /></td>
					</tr>
				</table>
				<div style="clear: both"></div>
				<hr />
				<input type="button" value="保存" onclick="doSubmit();"
					class="btn btn-default">
			</form>
		</div>
	</div>
	<script type="text/javascript">
		function doSubmit() {
			var data = $("#form").serialize();
			$.ajax({
				url : "add",
				data : data,
				type : 'post',
				contentType : 'application/x-www-form-urlencoded',
				encoding : 'UTF-8',
				cache : false,
				success : function(result) {
					if (result.success) {
						layer.alert('操作成功', {
							icon : 6
						},
								function() {
									var index = parent.layer
											.getFrameIndex(window.name); //获取窗口索引
									parent.layer.close(index);
								});
						parent.reload();
					} else {
						layer.alert('操作失败:' + result.obj);
					}
				},
				error : function() {
					layer.alert('系统异常');
				}
			});
		}
	</script>
</body>
</html>